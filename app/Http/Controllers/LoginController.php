<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Session;
use Illuminate\Http\RedirectResponse;

class LoginController extends Controller
{
    //
    public function form() {
        $sessionuser=Session::get('user');

        if ($sessionuser) {
            return redirect('/dashboard');
        }
        return view('login');
    }

    public function evallogin(Request $request) {
        $this->validate(request(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $email = $request->email;
        $password = $request->password;
        $user = DB::table('users')->where('email', $email)->first();
        if ($user) {
            if (sha1($password)==$user->password) {
                Session::put('user', $user);
                $gsessionuser = Session::get('user');
                // echo $gsessionuser->name;
                // die();
                return redirect('/dashboard');
            } else {
                $message = 'Invalid credentials';
                return redirect('/login')->with('warning', $message);
            }
        } else {
            $message = 'User does not exist';
            return redirect('/login')->with('warning', $message);
        }
    }

}
