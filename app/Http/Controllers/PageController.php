<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Http\RedirectResponse;
use Auth;
class PageController extends Controller
{
    //
    public function home() {
        $sessionuser=Session::get('user');
        if ($sessionuser) {
            return redirect('/dashboard');
        } else {
        return view('welcome');
        }
    }
    public function dashboard() {
        $sessionuser=Session::get('user');
        if ($sessionuser) {
            if(empty($sessionuser->verified)) {
                return redirect('/login')->with('warning', 'Please verify your email!');
            }
            return view('dashboard',['name' => $sessionuser->name, 'email' => $sessionuser->email, 'created'=>$sessionuser->created_at]);
        } else {
        return redirect('/login')->with('warning', 'Please login to access your dashboard');
        }
    }
    public function logout() {
        Auth::logout();
        Session::flush();
        return redirect('/login')->with('status', 'Successfully logged out!');
    }
}
