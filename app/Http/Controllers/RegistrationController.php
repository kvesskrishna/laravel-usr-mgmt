<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\VerifyUser;
use Mail;
use App\Mail\SendMailable;
use Session;
use Illuminate\Http\RedirectResponse;
use Auth;
class RegistrationController extends Controller
{
    //
    public function create() {
        $sessionuser=Session::get('user');

        if ($sessionuser) {
            return redirect()->to('/dashboard');
        }

        return view('registration.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6'
        ]);


        $user = User::create(request(['name', 'email', 'password']));
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => sha1(time())
          ]);


        Mail::to($user->email)->send(new SendMailable($user));

        // return $user;

        auth()->login($user);

        return redirect()->to('/registered');
    }

    public function registered() {
        return view('registration.registered');
    }

    public function verifyUser($token)
    {
      $verifyUser = VerifyUser::where('token', $token)->first();
      if(isset($verifyUser) ){
        $user = $verifyUser->user;
        if(!$user->verified) {
          $verifyUser->user->verified = 1;
          $verifyUser->user->save();
          $status = "Your e-mail is verified. You can now login.";
        } else {
          $status = "Your e-mail is already verified. You can now login.";
        }
      } else {
        return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
      }
      return redirect('/login')->with('status', $status);
    }
}
