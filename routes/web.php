<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home');
Route::get('/register', 'RegistrationController@create');
Route::post('register', 'RegistrationController@store');
Route::get('/registered', 'RegistrationController@registered');
Route::get('/user/verify/{token}', 'RegistrationController@verifyUser');
Route::get('/login', 'LoginController@form');
Route::post('/login', 'LoginController@evallogin');
Route::get('/dashboard', 'PageController@dashboard');
Route::get('/logout', 'PageController@logout');

