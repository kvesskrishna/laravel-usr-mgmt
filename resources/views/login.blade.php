@extends('layout')
@section('title')
Login
@endsection
@section('content')
<div class="row">
    @if (session('status'))
    <div class="col-12">
            <div class="alert alert-success">
                    {{ session('status') }}
                </div>
    </div>
    @endif
    @if (session('warning'))
    <div class="col-12">
            <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
    </div>
    @endif

    <div class="col-12">
        <h3 class="text-center">User Login</h3>
        @if (count($errors) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
      @endif
    </div>
</div>
<div class="row">
    <div class="col-6 offset-3 rform">
        <form action="/login" method="POST">
            {{ csrf_field() }}
            <div>
                <label for="fullname">Email</label>
                <input type="text" class="form-control" id="email" name="email">
            </div>
            <div>
                <label for="fullname">Password</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
            <br>
            <div class="text-center">
                <button class="btn btn-primary">Login</button>
            </div>
        </form>
    </div>
</div>
@endsection
