@extends('layout')
@section('title')
Dashboard
@endsection
@section('content')
<div class="row">
        <div class="col-12 text-right">
            Welcome {{$name}},
            <a href="/logout">Logout</a>
        </div>
    </div>

<div class="row">
        <div class="col-12">
                <div class="h3 text-center">
                    Dashboard
                </div>
            </div>
</div>
<div class="row">
    <div class="col-6 offset-3">
        <table class="table table-dark table-hover">
            <tr>
                <td>Name</td>
                <td>{{$name}}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>{{$email}}</td>
            </tr>
            <tr>
                <td>Registered on</td>
                <td>{{$created}}</td>
            </tr>
        </table>

    </div>
</div>
@endsection
