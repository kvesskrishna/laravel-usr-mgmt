@extends('layout')
@section('title')
Registration
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <h3 class="text-center">User Registration</h3>
        @if (count($errors) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
      @endif
    </div>
</div>
<div class="row">
    <div class="col-6 offset-3 rform">
        <form action="/register" method="POST">
            {{ csrf_field() }}
            <div>
                <label for="fullname">Full Name</label>
                <input type="text" class="form-control" id="fullname" name="name">
            </div>
            <div>
                <label for="fullname">Email</label>
                <input type="text" class="form-control" id="email" name="email">
            </div>
            <div>
                <label for="fullname">Password</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
            <div>
                <label for="password_confirmation">Confirm Password</label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
            </div>
            <br>
            <div class="text-center">
                <button class="btn btn-success">Register</button>
            </div>
        </form>
    </div>
</div>
@endsection
