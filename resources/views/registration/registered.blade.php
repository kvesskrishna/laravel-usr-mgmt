@extends('layout')
@section('title')
Registration
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <h3 class="text-center text-success">User Registered Successfully</h3>
        <br>
        <div class="tect-center">
            Email sent with verification link, please verify your email to login.
        </div>
    </div>
</div>
@endsection
